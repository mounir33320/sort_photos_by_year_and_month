<?php
const COPY_FUNCTION = "copyFile";
const MOVE_FUNCTION = "moveFile";

$choices = [COPY_FUNCTION, MOVE_FUNCTION];
echo "[0] Copier \n\r[1] Déplacer\n";
$choiceNumber = (int) readline("Choisissez une option avec le numéro adapté : ");
$chosenAction = array_key_exists($choiceNumber, $choices) ? $choices[$choiceNumber] : $choices[0];

$srcFolderPath = readline("Entrer le chemin du dossier à traiter : ");
$rootDirectory = "Résultat";
$elements = scandir($srcFolderPath);
$totalElements = count($elements);
$counter = 1;

function createDirectory(string $dirName){
    if(!file_exists($dirName)){
        mkdir($dirName);
    }
}

function copyFile(string $srcFile, string $destFile){
    if(!file_exists($destFile)){
        copy($srcFile, $destFile);
    }
}

function moveFile(string $srcFile, string $destFile){
    if(!file_exists($destFile)){
        rename($srcFile, $destFile);
    }
    else{
        unlink($srcFile);
    }
}

createDirectory($rootDirectory);
createDirectory($rootDirectory. "\\photos");

foreach($elements as $element){
    echo $counter. "/" .$totalElements. "\n";
    $fullpathElement = $srcFolderPath."\\" .$element;

    if(is_file($fullpathElement)){
        if(str_ends_with(strtolower($element),"jpeg") || str_ends_with(strtolower($element),"jpg")){
            $img = exif_read_data($fullpathElement);

            if(!isset($img["DateTime"]) && !isset($img["DateTimeOriginal"]) && !isset($img["DateTimeDigitized"])){
                createDirectory($rootDirectory. "\\images_sans_date");
                $chosenAction($fullpathElement, $rootDirectory. "\\images_sans_date\\" .$element);
            }
            else{
                if(isset($img["DateTimeOriginal"])){
                    $date = new DateTime($img["DateTimeOriginal"]);
                }
                elseif (isset($img["DateTimeDigitized"])){
                    $date = new DateTime($img["DateTimeDigitized"]);
                }
                else{
                    $date = new DateTime($img["DateTime"]);
                }

                $yearFolder = $date->format("Y");
                $monthFolder = $date->format("Y") . "\\" .$date->format("m");

                createDirectory($rootDirectory. "\\photos\\" .$yearFolder);
                createDirectory($rootDirectory. "\\photos\\" .$monthFolder);

                $chosenAction($fullpathElement, $rootDirectory. "\\photos\\" .$monthFolder. "\\" .$element);

            }
        }
        elseif(
            str_ends_with(strtolower($element), "webm") ||
            str_ends_with(strtolower($element), "mpg") ||
            str_ends_with(strtolower($element), "mp2") ||
            str_ends_with(strtolower($element), "mpeg") ||
            str_ends_with(strtolower($element), "mpv") ||
            str_ends_with(strtolower($element), "ogg") ||
            str_ends_with(strtolower($element), "mp4") ||
            str_ends_with(strtolower($element), "avi") ||
            str_ends_with(strtolower($element), "wmv") ||
            str_ends_with(strtolower($element), "mov") ||
            str_ends_with(strtolower($element), "3gp") ||
            str_ends_with(strtolower($element), "flv")
        ){
            createDirectory($rootDirectory. "\\videos");
            $chosenAction($fullpathElement, $rootDirectory. "\\videos\\" .$element);
        }
        else{
            createDirectory($rootDirectory. "\\fichiers_non_traites");
            $chosenAction($fullpathElement, $rootDirectory. "\\fichiers_non_traites\\" .$element);
        }

    }
    $counter++;
}
